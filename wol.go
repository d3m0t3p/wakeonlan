package main

import (
	"encoding/hex"
	"fmt"
	"net"
	"os"
)

func usage() {
	fmt.Printf("usage : %s mac", os.Args[0])
}

func main() {
	var mac string

	if len(os.Args) > 2 {
		usage()
	} else if len(os.Args) == 2 {
		mac = os.Args[1]
	} else {
		mac = "F46D04ED6E96" //my mac addr
	}

	ip := "255.255.255.255"
	endpoint := fmt.Sprintf("%s:9", ip)
	conn, _ := net.Dial("udp", endpoint)

	defer conn.Close()

	var buff []byte
	header, _ := hex.DecodeString("FFFFFFFFFFFF") //no error
	buff = append(buff, header...)

	macByted, _ := hex.DecodeString(mac)

	for i := 0; i < 16; i++ {
		buff = append(buff, macByted...)
	}
	conn.Write(buff)

}
